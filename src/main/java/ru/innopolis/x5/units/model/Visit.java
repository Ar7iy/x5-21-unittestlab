package main.java.ru.innopolis.x5.units.model;

public class Visit {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Student student;
    private Integer grade;
    private Lesson lesson;

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Visit() {
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Visit(Integer id, Lesson lesson, Student student, Integer grade) {
        this.id = id;
        this.lesson = lesson;
        this.student = student;
        this.grade = grade;
    }
}
