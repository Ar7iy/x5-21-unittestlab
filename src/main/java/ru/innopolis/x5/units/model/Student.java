package main.java.ru.innopolis.x5.units.model;

public class Student {
    private Integer id;
    private String fio;
    private Integer group;

    public Student() {
    }

    public Student(Integer id, String fio, Integer group) {
        this.id = id;
        this.fio = fio;
        this.group = group;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", group=" + group +
                '}';
    }
}
