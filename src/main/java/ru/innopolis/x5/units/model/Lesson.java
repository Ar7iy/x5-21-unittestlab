package main.java.ru.innopolis.x5.units.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lesson {
    private Integer id;
    private String topic;
    private Group group;
    private List<Visit> visits;

    public Lesson() {
    }

    public Lesson(Integer id, String topic, Group group) {
        this.id = id;
        this.topic = topic;
        this.group = group;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Visit> getVisits() {
        if (visits == null) {
            return Collections.emptyList();
        }
        return visits;
    }

    public void addVisit(Visit visit) {
        if (visits == null) {
            visits = new ArrayList<>();
        }
        visits.add(visit);
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", group=" + group +
                '}';
    }
}
