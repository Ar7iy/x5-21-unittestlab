package main.java.ru.innopolis.x5.units;

import main.java.ru.innopolis.x5.units.dao.StudentDaoSimpleImpl;
import main.java.ru.innopolis.x5.units.service.StudentService;

public class Main {
    public static void main(String[] args) {
        StudentService studentService = new StudentService(new StudentDaoSimpleImpl());
        System.out.println(studentService.averageGrade(1));
        System.out.println(studentService.vowelStudents());
    }
}
