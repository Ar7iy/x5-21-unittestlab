package main.java.ru.innopolis.x5.units.service;

import main.java.ru.innopolis.x5.units.dao.StudentDao;
import main.java.ru.innopolis.x5.units.model.Lesson;
import main.java.ru.innopolis.x5.units.model.Student;
import main.java.ru.innopolis.x5.units.model.Visit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class StudentService {
    private StudentDao studentDao;

    public StudentService(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public Double averageGrade(Integer lessonId) {
        Integer sum = 0;

        Lesson lesson = studentDao.getLesson(lessonId);
        if (lesson == null) {
            return 0.0d;
        }

        List<Visit> visits = studentDao.getLesson(lessonId).getVisits();
        if (visits == null) {
            return 0.0d;
        }

        for (Visit visit: visits) {
            sum += visit.getGrade();
        }
        return (double) sum / (studentDao.getAllLessons().size() == 0 ? 1 : studentDao.getAllLessons().size());
    }

    public List<Student> vowelStudents() {
        String vowels = "EeYyUuIiOoAa";
        List<Student> result = new ArrayList<>();

        List<Student> students = studentDao.getAllStudents();
        if (students == null) {
            return Collections.emptyList();
        }

        for (Student student: students) {
            if (vowels.indexOf(student.getFio().toCharArray()[0]) >= 0)  {
                result.add(student);
            }
        }
        return result;
    }
}
