package main.java.ru.innopolis.x5.units.service.mapper;

import main.java.ru.innopolis.x5.units.model.Lesson;
import main.java.ru.innopolis.x5.units.outermodel.IncomingVisit;

public interface IncomingVisitToLessonMapper {
    Lesson mapIncomingVisitToLesson(IncomingVisit incomingVisit);
}
