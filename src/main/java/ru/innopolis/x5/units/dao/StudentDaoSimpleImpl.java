package main.java.ru.innopolis.x5.units.dao;

import main.java.ru.innopolis.x5.units.model.Group;
import main.java.ru.innopolis.x5.units.model.Lesson;
import main.java.ru.innopolis.x5.units.model.Student;
import main.java.ru.innopolis.x5.units.model.Visit;
import java.util.*;

public class StudentDaoSimpleImpl implements StudentDao {
    Map<Integer, Group> groups = new HashMap<>();
    Map<Integer, Student> students = new HashMap<>();
    Map<Integer, Lesson> lessons = new HashMap<>();
    Map<Integer, Visit> visits = new HashMap<>();

    public StudentDaoSimpleImpl() {
        groups.put(1, new Group(1, "first group"));
        groups.put(2, new Group(2, "second group"));

        students.put(1, new Student(1, "AFirst Student", 1));
        students.put(2, new Student(2, "Second Student", 1));
        students.put(3, new Student(3, "Third Student", 2));
        students.put(4, new Student(4, "Fourth Student", 2));

        groups.get(1).addStudent(students.get(1));
        groups.get(1).addStudent(students.get(2));
        groups.get(2).addStudent(students.get(3));
        groups.get(2).addStudent(students.get(4));

        lessons.put(1, new Lesson(1, "Lesson One", groups.get(1)));
        lessons.put(2, new Lesson(2, "Lesson Two", groups.get(1)));
        lessons.put(3, new Lesson(3, "Lesson Three", groups.get(2)));
        lessons.put(4, new Lesson(4, "Lesson Four", groups.get(2)));

        visits.put(1, new Visit(1, lessons.get(1), students.get(1), 4));
        visits.put(2, new Visit(2, lessons.get(1), students.get(1), 5));
        visits.put(3, new Visit(3, lessons.get(2), students.get(2), 4));
        visits.put(4, new Visit(4, lessons.get(2), students.get(2), 5));
        visits.put(5, new Visit(5, lessons.get(3), students.get(3), 4));
        visits.put(6, new Visit(6, lessons.get(3), students.get(3), 5));
        visits.put(7, new Visit(7, lessons.get(4), students.get(4), 4));
        visits.put(8, new Visit(8, lessons.get(4), students.get(4), 5));

        lessons.get(1).addVisit(visits.get(1));
        lessons.get(1).addVisit(visits.get(2));
        lessons.get(2).addVisit(visits.get(3));
        lessons.get(2).addVisit(visits.get(4));
        lessons.get(3).addVisit(visits.get(5));
        lessons.get(3).addVisit(visits.get(6));
        lessons.get(4).addVisit(visits.get(7));
        lessons.get(4).addVisit(visits.get(8));
    }

    @Override
    public List<Lesson> getAllLessons() {
        return new ArrayList<>(this.lessons.values());
    }

    @Override
    public Lesson getLesson(Integer lessonId) {
        return lessons.get(lessonId);
    }

    @Override
    public void addStudent(Student student) {
        students.put(student.getId(), student);
    }

    @Override
    public void addLesson(Lesson lesson) {
        lessons.put(lesson.getId(), lesson);
    }

    @Override
    public void addVisit(Visit visit) {
        visits.put(visit.getId(), visit);
    }

    @Override
    public List<Student> getAllStudents() {
        return new ArrayList<>(students.values());
    }
}
