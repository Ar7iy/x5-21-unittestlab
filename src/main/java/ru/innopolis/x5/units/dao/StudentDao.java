package main.java.ru.innopolis.x5.units.dao;

import main.java.ru.innopolis.x5.units.model.Lesson;
import main.java.ru.innopolis.x5.units.model.Student;
import main.java.ru.innopolis.x5.units.model.Visit;

import java.util.List;

public interface StudentDao {
    List<Lesson> getAllLessons();
    List<Student> getAllStudents();

    Lesson getLesson(Integer lessonId);

    void addStudent(Student student);

    void addLesson(Lesson lesson);

    void addVisit(Visit visit);

}
