package main.java.ru.innopolis.x5.units.service;

import main.java.ru.innopolis.x5.units.dao.StudentDao;
import main.java.ru.innopolis.x5.units.model.Lesson;
import main.java.ru.innopolis.x5.units.model.Visit;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class StudentServiceTest {
    private StudentService studentService;
    @Mock private StudentDao studentDao;
    @Mock private Lesson lesson;
    @Mock private List<Visit> visits;

    @Before
    public void setUp() {
        initMocks(this);
        this.studentService = new StudentService(studentDao);
    }

    @Test
    public void studentDaoGetLessonReturnNull() {
        when(studentDao.getLesson(any())).thenReturn(null);
        Double result = studentService.averageGrade(1);
        assertEquals(result, 0.0d);
    }

    @Test
    public void lessonGetVisitsReturnNull() {
        when(studentDao.getLesson(any())).thenReturn(lesson);
        when(lesson.getVisits()).thenReturn(null);
        Double result = studentService.averageGrade(1);
        assertEquals(result, 0.0d);
    }

}
